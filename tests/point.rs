use datagen_gnss::elements::point::Point;
use datagen_gnss::elements::value_pair::ValuePair;
#[macro_use]
extern crate serde_json;

/*
tests:

- origin
- new
- size
- distance
- json_dumps
- json_loads
 */

#[test]
fn create_origin() {
    let vp = Point::origin();
    let empty = ValuePair::empty();
    assert_eq!(vp.x + vp.y + vp.z, empty);
}

#[test]
fn create_new() {
    let x1 = 4.3;
    let x2 = 89.3;
    let x3 = -12.3;
    let e1 = 0.03;
    let e2 = 0.21;
    let e3 = 0.023;
    let vp1 = ValuePair::new(x1, e1);
    let vp2 = ValuePair::new(x2, e2);
    let vp3 = ValuePair::new(x3, e3);
    let vpsum = vp1 + vp2 + vp3;
    let point = Point::new(vp1, vp2, vp3);
    assert_eq!(point.x + point.y + point.z, vpsum);
}

#[test]
fn get_size() {
    let x1 = 4.3;
    let x2 = 89.3;
    let x3 = -12.3;
    let e1 = 0.03;
    let e2 = 0.21;
    let e3 = 0.023;
    let vp1 = ValuePair::new(x1, e1);
    let vp2 = ValuePair::new(x2, e2);
    let vp3 = ValuePair::new(x3, e3);
    let size_vp = (vp1.pow(2) + vp2.pow(2) + vp3.pow(2)).sqrt();
    let point = Point::new(vp1, vp2, vp3);
    assert_eq!(point.size(), size_vp);
}

#[test]
fn serialize() {
    /*
    Create a Point, serialize, compare with a json string.
     */
    let x1 = 4.3;
    let x2 = 89.3;
    let x3 = -12.3;
    let e1 = 0.03;
    let e2 = 0.21;
    let e3 = 0.023;
    let vp1 = ValuePair::new(x1, e1);
    let vp2 = ValuePair::new(x2, e2);
    let vp3 = ValuePair::new(x3, e3);
    let json_data = json!(
    {
        "x":{
            "error":e1,
            "value":x1
         },
        "y":{
            "error":e2,
            "value":x2
         },
        "z":{
            "error":e3,
            "value":x3
         },
    });
    let json_pretty = serde_json::to_string_pretty(&json_data).unwrap();
    let point = Point::new(vp1, vp2, vp3);
    let point_json = point.json_dumps();

    // let point_json_plain = str::replace(point_json, " ", "");
    assert_eq!(point_json, json_pretty);
}

#[test]
fn deserialize() {
    /*
    bring some json string structured as Point to Point
     */
    let x1 = 4.3;
    let x2 = 89.3;
    let x3 = -12.3;
    let e1 = 0.03;
    let e2 = 0.21;
    let e3 = 0.023;
    let vp1 = ValuePair::new(x1, e1);
    let vp2 = ValuePair::new(x2, e2);
    let vp3 = ValuePair::new(x3, e3);
    let point = Point::new(vp1, vp2, vp3);
    let json_data = json!(
    {
        "x":{
            "error":e1,
            "value":x1
        },
        "y":{
            "error":e2,
            "value":x2
        },
        "z":{
            "error":e3,
            "value":x3
        },
    });
    let json_pretty = serde_json::to_string_pretty(&json_data).unwrap();
    let point_from_json = Point::json_loads(&json_pretty);
    assert_eq!(point, point_from_json);
}
