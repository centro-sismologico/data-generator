use datagen_gnss::elements::data::Data;
use datagen_gnss::stream::read::{read_file, read_io}; //, read_io};
use std::io::{Cursor};
use std::path::Path;
use seek_bufread::BufReader;

#[macro_use]
extern crate serde_json;

#[test]
fn test_stream_read() {
    /*
    create bytestring of the json
    load it to a Vec
    load vect to string
     */
    let json_string = r#"
     {
       "dt_gen": "14/07/2021T20:31:36.035497841UTC",
       "origin": {
         "name": "TestData",
         "serie_id": 1
       },
       "point": {
         "x": {
           "error": 0.34741300846241663,
           "value": 2.2568976794321935
         },
         "y": {
           "error": 0.2550604766668364,
           "value": 6.070757249626687
         },
         "z": {
           "error": 0.09804820380025447,
           "value": -1.8146543294125694
         }
       },
       "protocol": "gendata",
       "version": "v2021.06.25"
     }
     *end*
     {
     "dt_gen": "14/07/2021T22:21:44.025269550UTC",
     "origin": {
         "name": "TestData",
         "serie_id": 1
     },
     "point": {
         "x": {
             "error": 0.0727939934743338,
             "value": 3.1540975450997486
         },
         "y": {
             "error": 0.15367453769215977,
             "value": 5.83866050014176
         },
         "z": {
             "error": 0.24111259676388475,
             "value": -2.551320555652193
         }
     },
     "protocol": "gendata",
     "version": "v2021.06.25"
     }
     *end*
     {
          "dt_gen": "14/07/2021T22:21:58.733549006UTC",
          "origin": {
              "name": "TestData",
              "serie_id": 1
          },
          "point": {
              "x": {
                  "error": 0.016094159955821308,
                  "value": 2.5894686789854475
              },
              "y": {
                  "error": 0.3702754850089164,
                  "value": 6.086119138027
              },
              "z": {
                  "error": 0.45935171317077694,
                  "value": -1.8043056174457144
              }
          },
          "protocol": "gendata",
          "version": "v2021.06.25"
      }
      *end*
    "#;
    
    // let mut reader = BufReader::new(
    //     cursor);
    let cursor = Cursor::new(json_string.as_bytes());
    let end_flag = String::from("*end*");
    //let list:Vec<Data> =
    let dataset: Vec<_> = json_string.split("*end*")
        .filter(|&x| !x.trim().is_empty())
        .map(|json| {
            Data::json_loads(&json)
        }).collect();
    // for (i, item) in dataset.iter().enumerate() {
    //     println!("{} -> {}", i, item);
    // }

    //Data::json_loads(&json)
    //.collect();
    /*
    Se trata de ingresar el stream,
    y comparar con los data generados directamente.
     */

    // the bytes are charged correctly into the cursor
    // println!("cursor position {}", buffer.position());
    // for elem  in buffer.clone().into_inner().iter() {
    //     println!("{}", elem)
    // }

    let mut reader = BufReader::new(cursor);
    let data_result = read_io(
        &mut reader,
        &end_flag,
        true);

    let mut counter = 0;

    match data_result {
        Ok(vector) => {
            for (i, item) in vector.iter().enumerate() {
                let y =  &dataset[i];
                if item == y {
                    counter += 1;
                }
            }
        },
        Err(e) => println!("{}", e)
    }

    /*
    hacer un all -> zip a==b
    comparar punto a punto y que todos sean iguales
     */
    // for (i, d) in data_result.iter().enumerate() {
    //     let y =  &dataset[i];
    //     if d == y {
    //         counter += 1;
    //     }
    // }

    assert_eq!(dataset.len(), counter)
}

#[test]
fn test_read_file() {
    // from json array
    let json = json!([
           {
               "dt_gen": "14/07/2021T20:31:36.035497841UTC",
               "origin": {
                   "name": "TestData",
                   "serie_id": 1
               },
               "point": {
                   "x": {
                       "error": 0.34741300846241663,
                       "value": 2.2568976794321935
                   },
                   "y": {
                       "error": 0.2550604766668364,
                       "value": 6.070757249626687
                   },
                   "z": {
                       "error": 0.09804820380025447,
                       "value": -1.8146543294125694
                   }
               },
               "protocol": "gendata",
               "version": "v2021.06.25"
           },
    {
        "dt_gen": "14/07/2021T22:21:44.025269550UTC",
        "origin": {
            "name": "TestData",
            "serie_id": 1
        },
        "point": {
            "x": {
                "error": 0.0727939934743338,
                "value": 3.1540975450997486
            },
            "y": {
                "error": 0.15367453769215977,
                "value": 5.83866050014176
            },
            "z": {
                "error": 0.24111259676388475,
                "value": -2.551320555652193
            }
        },
        "protocol": "gendata",
        "version": "v2021.06.25"
    },
    {
        "dt_gen": "14/07/2021T22:21:58.733549006UTC",
        "origin": {
            "name": "TestData",
            "serie_id": 1
        },
        "point": {
            "x": {
                "error": 0.016094159955821308,
                "value": 2.5894686789854475
            },
            "y": {
                "error": 0.3702754850089164,
                "value": 6.086119138027
            },
            "z": {
                "error": 0.45935171317077694,
                "value": -1.8043056174457144
            }
        },
        "protocol": "gendata",
        "version": "v2021.06.25"
    }]
       );
    let json_pretty = serde_json::to_string_pretty(
        &json).unwrap();
    let data_from_code: Vec<Data> = serde_json::from_str(
        &json_pretty).unwrap();
    // from file:
    let path = Path::new("./tests/files/data_one.json");
    let data = read_file(
        path).unwrap();
    let data_json = serde_json::to_string_pretty(
        &data).unwrap();
    let data_from_file: Vec<Data> = serde_json::from_str(
        &data_json).unwrap();

    assert_eq!(data_from_code, data_from_file);
}
