use datagen_gnss::elements::equipment::Equipment;
#[macro_use]
extern crate serde_json;
/*
tests:

- new
- json_dumps -> serialize
- json_loads -> deserialize

 */

#[test]
fn create() {
    let name = String::from("test equipment");
    let serie_id = 101;
    let name_copy = name.clone();
    let equipment = Equipment::new(name, serie_id);
    assert!(equipment.name == name_copy && equipment.serie_id == serie_id);
}

#[test]
fn serialize() {
    /*
    convert to string
     */
    let name = String::from("test equipment");
    let serie_id = 101;
    let json_data = json!(
    {
        "name":name,
        "serie_id":serie_id
    });
    let json_pretty = serde_json::to_string_pretty(&json_data).unwrap();
    let equipment = Equipment::new(name, serie_id);
    let equipment_json = equipment.json_dumps();
    assert_eq!(json_pretty, equipment_json);
}

#[test]
fn deserialize() {
    let name = String::from("test equipment");
    let serie_id = 101;
    let json_data = json!(
    {
        "name":name,
        "serie_id":serie_id
    });
    let json_pretty = serde_json::to_string_pretty(&json_data).unwrap();
    let new_equipment = Equipment::json_loads(&json_pretty);
    let equipment = Equipment::new(name, serie_id);
    assert_eq!(new_equipment, equipment);
}
