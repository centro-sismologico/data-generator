use chrono::{DateTime, Utc};
use datagen_gnss::elements::point::Point;
use datagen_gnss::elements::{data::Data, equipment::Equipment};
use datagen_gnss::generators;

#[doc = r#"
Check random value, generate a value between min and max
do a 'lot' of evaluation to check statistically
"#]
#[test]
fn test_random_value() {
    let min = -1000.0;
    let max = 1000.0;
    let mut counter = 0;
    let amount = 1000000;
    for _ in 0..amount {
        let newvalue = generators::random_value(min, max);
        if min <= newvalue && newvalue <= max {
            counter += 1;
        }
    }
    assert!(counter == amount);
}

#[test]
fn test_position_and_errors() {
    let new_point = generators::position_and_errors();
    let point = Point::origin();
    assert_ne!(point, new_point)
}

#[test]
fn test_create_data() {
    let protocol: String = String::from("gendata");
    let version: String = String::from("v2021.06.25");
    let equipment_name: String = String::from("TestData");
    let id = 100;
    let equipment: Equipment = Equipment::new(equipment_name, id);
    let random_data = generators::create_data(
        &protocol,
        &version,
        &equipment);
    let point = Point::origin();
    let dt_gen: DateTime<Utc> = Utc::now();
    let data = Data::new(
        &protocol,
        &version,
        &equipment, dt_gen, point);
    assert!(data.protocol == random_data.protocol && data.version == random_data.version);
}
