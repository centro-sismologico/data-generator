use datagen_gnss::elements::value_pair::ValuePair;

/*
Create an empty valuepair is (0,0)
 */
#[test]
fn create_empty() {
    let vp = ValuePair::empty();
    assert_eq!(vp.value + vp.error, 0.0);
}

#[test]
fn create_new() {
    let value: f64 = 113.4;
    let error: f64 = 0.04;
    let vp = ValuePair::new(value, error);
    assert_eq!(vp.value + vp.error, value + error);
}

#[test]
fn serialize() {
    let value: f64 = 2.927140579832943;
    let error: f64 = 0.3108550010025529;
    let json_data = r#"{
                      "value":2.927140579832943 ,
                      "error":0.3108550010025529
                    }"#;

    let new_data: ValuePair = ValuePair::json_loads(json_data);
    let new_data_json = new_data.json_dumps();

    let data: ValuePair = ValuePair::new(value, error);
    let data_json = data.json_dumps();

    assert_eq!(new_data_json, data_json);
}

#[test]
fn deserializer() {
    let value: f64 = 2.927140579832943;
    let error: f64 = 0.3108550010025529;
    let json_data = r#"{
                      "value":2.927140579832943 ,
                      "error":0.3108550010025529
                    }"#;
    let new_data: ValuePair = ValuePair::json_loads(json_data);
    let new_data_ok: ValuePair = ValuePair::new(value, error);
    assert_eq!(new_data, new_data_ok);
}

/*
test operations + - * /

check error propagation implementation
 */

#[test]
fn adding() {
    let x1 = 4.3;
    let e1 = 0.03;
    let x2 = 67.3;
    let e2 = 0.014;
    let vp1 = ValuePair::new(x1, e1);
    let vp2 = ValuePair::new(x2, e2);
    let add = vp1 + vp2;
    assert!(add.value == (x1 + x2) && add.error == (e1 + e2));
}

#[test]
fn substracting() {
    let x1 = 4.3;
    let e1 = 0.03;
    let x2 = 67.3;
    let e2 = 0.014;
    let vp1 = ValuePair::new(x1, e1);
    let vp2 = ValuePair::new(x2, e2);
    let add = vp1 - vp2;
    assert!(add.value == (x1 - x2) && add.error == (e1 + e2));
}

#[test]
fn multiply() {
    let x1 = 4.3;
    let e1 = 0.03;
    let x2 = 67.3;
    let e2 = 0.014;
    let vp1 = ValuePair::new(x1, e1);
    let vp2 = ValuePair::new(x2, e2);
    let add = vp1 * vp2;
    let x1x2 = x1 * x2;
    let errr =
        (x1x2.abs()) * ((e1 / x2).powf(2.0) + (e2 / x1).powf(2.0) + 2. * (e1 * e2) / (x1x2)).sqrt();
    assert!(add.value == (x1 * x2) && add.error == errr);
}

#[test]
fn division() {
    let x1 = 4.3;
    let e1 = 0.03;
    let x2 = 67.3;
    let e2 = 0.014;
    let vp1 = ValuePair::new(x1, e1);
    let vp2 = ValuePair::new(x2, e2);
    let add = vp1 / vp2;
    let x1_div_x2 = x1 / x2;
    let x1x2 = x1 * x2;
    let errr = (x1_div_x2.abs())
        * ((e1 / x2).powf(2.0) + (e2 / x1).powf(2.0) - 2. * (e1 * e2) / (x1x2)).sqrt();
    assert!(add.value == (x1 / x2) && add.error == errr);
}

#[test]
#[should_panic]
fn division_by_zero() {
    let x1 = 4.3;
    let e1 = 0.03;
    let x2 = 0.0;
    let e2 = 0.014;
    let vp1 = ValuePair::new(x1, e1);
    let vp2 = ValuePair::new(x2, e2);
    let _add = vp1 / vp2;
    let x1_div_x2 = x1 / x2;
    let _x1x2 = x1 * x2;
    let _errr = (x1_div_x2.abs())
        * ((e1 / x2).powf(2.0) + (e2 / x1).powf(2.0) - 2. * (e1 * e2) / (_x1x2)).sqrt();
}

#[test]
fn value_zero() {
    let x1 = 0.0;
    let e1 = 0.03;
    let vp1 = ValuePair::new(x1, e1);
    assert_eq!(vp1.is_zero(), true);
}

#[test]
fn get_sqrt() {
    let x1 = 4.0;
    let e1 = 0.03;
    let vp1 = ValuePair::new(x1, e1);
    let value_sqrt = x1.sqrt();
    let error_sqrt = value_sqrt * (0.5 * e1) / (x1);
    assert_eq!(vp1.sqrt(), ValuePair::new(value_sqrt, error_sqrt));
}

#[test]
#[should_panic]
fn negative_value_sqrt() {
    let x1 = -4.0;
    let e1 = 0.03;
    let vp1 = ValuePair::new(x1, e1);
    vp1.sqrt();
}

/*

Check assign operators:

+=
*=
-=
/=

error mult:
let new_error = (new_value.abs())*(
(self.error / new_pair.value).powf(2.0) +
(new_pair.error/self.value).powf(2.0) +
2.*(self.error*new_pair.error)/(self.value*new_pair.value)).sqrt();


 */
#[test]
fn multiply_assign() {
    // createnew mutable value pair
    let x1 = 4.0;
    let e1 = 0.03;
    let mut vp1 = ValuePair::new(x1, e1);
    let x2 = 5.89;
    let e2 = 0.03;
    let vp2 = ValuePair::new(x2, e2);
    vp1 *= vp2;
    let x1x2 = x1 * x2;
    let error = (x1x2.abs())
        * ((e2 / x1).powf(2.0) + (e1 / x2).powf(2.0) + 2. * (e1 * e2) / (x1x2.abs())).sqrt();
    assert!(vp1.value == (x1 * x2) && vp1.error == error); //&& vp1.error==errr
}

#[test]
fn add_assign() {
    let x1 = 4.0;
    let e1 = 0.03;
    let mut vp1 = ValuePair::new(x1, e1);
    let x2 = 5.89;
    let e2 = 0.03;
    let vp2 = ValuePair::new(x2, e2);
    let vp3 = vp1 + vp2;
    vp1 += vp2;
    assert_eq!(vp1, vp3);
}

#[test]
fn division_assign() {
    // createnew mutable value pair
    let x1 = 4.0;
    let e1 = 0.03;
    let mut vp1 = ValuePair::new(x1, e1);
    let x2 = 5.89;
    let e2 = 0.03;
    let vp2 = ValuePair::new(x2, e2);
    vp1 /= vp2;
    let x1x2 = x1 / x2;
    let error = (x1x2.abs())
        * ((e2 / x1).powf(2.0) + (e1 / x2).powf(2.0) - 2. * (e1 * e2) / (x1 * x2.abs())).sqrt();
    assert!(vp1.value == (x1 / x2) && vp1.error == error); //&& vp1.error==errr
}

#[test]
fn sub_assign() {
    let x1 = 4.0;
    let e1 = 0.03;
    let mut vp1 = ValuePair::new(x1, e1);
    let x2 = 5.89;
    let e2 = 0.03;
    let vp2 = ValuePair::new(x2, e2);
    let vp3 = vp1 - vp2;
    vp1 -= vp2;
    assert_eq!(vp1, vp3);
}

/*


check pow
 */

#[test]
fn pow_vp() {
    let x1 = 4.0;
    let e1 = 0.03;
    let vp1 = ValuePair::new(x1, e1);
    let vp_2 = vp1 * vp1;
    let vppow2 = vp1.pow(2);
    let vp5 = ValuePair::new(x1, e1);
    let vp_5 = vp5 * vp5 * vp5 * vp5 * vp5;
    let vppow5 = vp5.pow(5);
    assert!(vppow2 == vp_2 && vppow5 == vp_5);
}
