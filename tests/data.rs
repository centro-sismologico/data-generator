use chrono::{DateTime, Utc};
use datagen_gnss::elements::data::Data;
use datagen_gnss::elements::equipment::Equipment;
use datagen_gnss::elements::point::Point;

#[macro_use]
extern crate serde_json;
const FORMAT: &str = "%d/%m/%YT%H:%M:%S%.f%Z";

/*
Tests

- new
- serialize
- deserialize
 */

#[test]
fn create() {
    let protocol = String::from("test_data");
    let version = String::from("0.1");
    let name = String::from("Test");
    let id = 101;
    let protocol_copy = protocol.clone();
    let version_copy = version.clone();
    let origin = Equipment::new(name, id);
    let origin_copy = origin.clone();
    let point = Point::origin();
    let dt_gen: DateTime<Utc> = Utc::now();
    let dt_gen_copy = dt_gen.clone();
    let point_b = Point::origin();
    let data = Data::new(
        &protocol,
        &version,
        &origin,
        dt_gen,
        point);
    assert!(
        data.protocol == protocol_copy
            && data.version == version_copy
            && data.origin == origin_copy
            && data.dt_gen == dt_gen_copy
            && data.point == point_b
    );
}

#[test]
fn serialize() {
    /*
    define parameters
    create a A=json!
    create a Data
    serialize Data
    serialize A
    compare A
     */
    let protocol = String::from("test_data");
    let version = String::from("0.1");
    let name = String::from("Test");
    let id = 101;
    let origin = Equipment::new(name, id);
    let point = Point::origin();
    let dt_gen: DateTime<Utc> = Utc::now();
    let dt_gen_format = format!("{}", dt_gen.format(FORMAT));
    let json_data = json!(
        {
            "dt_gen": dt_gen_format,
            "origin": origin,
            "point":{
                "x":{
                    "error":0.0,
                    "value":0.0
                },
                "y":{
                    "error":0.0,
                    "value":0.0
                },
                "z":{
                    "error":0.0,
                    "value":0.0
                },
            },
            "protocol": protocol,
            "version": version,
    });
    let json_pretty = serde_json::to_string_pretty(&json_data).unwrap();
    let data = Data::new(
        &protocol,
        &version,
        &origin, dt_gen, point);
    let data_json = data.json_dumps();
    assert_eq!(data_json, json_pretty);
}

#[test]
fn deserialize() {
    /*
    Create json and deserialize to Data
    Create Data
    compare data_json with data
     */
    let protocol = String::from("test_data");
    let version = String::from("0.1");
    let name = String::from("Test");
    let id = 101;
    let origin = Equipment::new(name, id);
    let point = Point::origin();
    let dt_gen: DateTime<Utc> = Utc::now();
    let dt_gen_format = format!("{}", dt_gen.format(FORMAT));
    let json_data = json!(
    {
        "dt_gen": dt_gen_format,
        "origin": origin,
        "point":{
            "x":{
                "error":0.0,
                "value":0.0
            },
            "y":{
                "error":0.0,
                "value":0.0
            },
            "z":{
                "error":0.0,
                "value":0.0
            },
        },
        "protocol": protocol,
        "version": version,
    });
    let json_pretty = serde_json::to_string_pretty(&json_data).unwrap();
    let data_json = Data::json_loads(&json_pretty);
    let data = Data::new(
        &protocol,
        &version,
        &origin, dt_gen, point);
    assert_eq!(data, data_json);
}
