
# Table of Contents

1.  [Data](#org63433df)
    1.  [ValuePair](#org38f4a8b)
    2.  [Point](#org1426c02)
    3.  [Equipment](#org3afe002)
    4.  [DT\_GEN Format](#org4f28bf5)
    5.  [Data](#org90c666d)
2.  [Random data gen](#orgabd0641)
3.  [Stream I/O](#org7b4d2ec)
    1.  [Lectura de archivos](#org188e324)
    2.  [Lectura de stream y entrega de vector](#orgc4c81f6)
    3.  [Lectura de stream y entrega a stream](#org7e82481)
4.  [Tests](#org6808af5)



<a id="org63433df"></a>

# Data

La estructura **Data** contiene toda la información que podría generar un sensor base.

Se necesita construir lo siguiente.


<a id="org38f4a8b"></a>

## DONE ValuePair


<a id="org1426c02"></a>

## DONE Point


<a id="org3afe002"></a>

## DONE Equipment


<a id="org4f28bf5"></a>

## DONE DT\_GEN Format


<a id="org90c666d"></a>

## DONE Data

Todo está definido bajo una estrucura de directorios.

    tree src


<a id="orgabd0641"></a>

# Random data gen

Es la serie de funciones que construyen aleatoriamente datos a
solicitud. 

En **generators.rs** tenemos la función **create\_data** que genera una
instancia de **Data** aleatoria.


<a id="org7b4d2ec"></a>

# Stream I/O

Creación de comando que permite una entrada y deserializarla. Sea un
stream o de un archivo.

Hay dos funciones **síncronas** que permite leer o escribir desde un
stream.

-   read\_io
-   write-io

De estas dos derivan las asociadas a archivos, para leer o escribir a
archivos.

-   read\_file
-   write\_file

Asímismo, de manera **asíncrona** se usa **tokio** para crear las
funciones que toman leen desde un **buffer** y lo envían por un canal y
viceversa.

-   render\_to\_stream
-   stream\_to\_writer

Estas funcionalidades son claves para que este módulo se pueda
utilizar, por ejemplo, en servidores tcp o servicios generadores de
datos para simular sensores.


<a id="org188e324"></a>

## Lectura de archivos

En **stream.rs** -> **read\_file** se implementa una función sencilla que toma un
archivo completo y lo deserializa directamente para convertirlo en un array de Data.

Esto es solo para un archivo de tipo **json**.

    pub fn read_file<P: AsRef<Path>>(
        path: P) -> Result<Vec<Data>, Box<dyn Error>> {
        // open file if exists path
        let file = File::open(path)?;
        // set file to buffer
        let reader = BufReader::new(file);
        // read the json contents
        let json_data = serde_json::from_reader(reader)?;
        // Return Data
        Ok(json_data)
    }


<a id="orgc4c81f6"></a>

## Lectura de stream y entrega de vector

Si el origen del dato viene de un stream, entonces se usa **read\_io** que toma la
serie de valores (o de un archivo no json)

    pub fn read_io<R: io::BufRead >(
        mut input: R,
        end_flag: &String,
        print: bool) -> Result<Vec<Data>, Box<dyn Error>> {
        let mut lines: Vec<String> = vec![];
        let mut dataset: Vec<Data> = vec![];
        for value in input.lines() {
            let line = value?;
              if line.as_str().trim() == end_flag   {
                  let new_value = lines.join("");
                  let new_data = Data::json_loads(&new_value);
                  if print {
                      println!("{}", new_data);
                  }
                  dataset.push(new_data);
                  lines.clear();
              } else {
                  if !line.trim().is_empty() {
                      lines.push(line);
                      }
              }
            }
        Ok(dataset)
    }


<a id="org7e82481"></a>

## Lectura de stream y entrega a stream

En este caso, en vez de lograr un acumulador, tomar el Data convertido enviarlo
mediante un stream con rasgo **Write**.

Por ejemplo, podría necesitar enviarlo por una cola a un stream de socket,
entonces crear un módulo que tome el dato, lo procese y envíe a otra parte, si
es que lo necesita. También podría utilizarse para enviar a un proceso
concurrente o funcionalidad atómica separada.


<a id="org6808af5"></a>

# Tests

Todas las operaciones disponibles para crear datos y enviarlos o
recibirlos por stream están testeadas.

Revisar los **tests** pueden servir para aprender a usar las funciones.

