use crate::elements::data::Data;
use crate::elements::equipment::Equipment;
use crate::elements::point::Point;
use crate::elements::value_pair::ValuePair;
use chrono::{DateTime, Utc};
use rand::distributions::{Distribution, Uniform};

pub fn random_value(min: f64, max: f64) -> f64 {
    let generator = Uniform::from(min..max);
    let mut rng = rand::thread_rng();
    generator.sample(&mut rng)
}

pub fn position_and_errors() -> Point {
    const X_MIN: f64 = 2.0;
    const X_MAX: f64 = 3.6;
    const Y_MIN: f64 = 4.5;
    const Y_MAX: f64 = 6.2;
    const Z_MIN: f64 = -3.4;
    const Z_MAX: f64 = -1.5;
    const ERR_MIN: f64 = 0.0;
    const ERR_MAX: f64 = 0.5;
    let x = random_value(X_MIN, X_MAX);
    let err_x = random_value(ERR_MIN, ERR_MAX);
    let y = random_value(Y_MIN, Y_MAX);
    let err_y = random_value(ERR_MIN, ERR_MAX);
    let z = random_value(Z_MIN, Z_MAX);
    let err_z = random_value(ERR_MIN, ERR_MAX);
    // POINT {x{value, err}, y{value, err}, z{value, err}}
    Point::new(
        ValuePair::new(x, err_x),
        ValuePair::new(y, err_y),
        ValuePair::new(z, err_z),
    )
}

pub fn create_data(
    protocol: &String,
    version: &String,
    origin: &Equipment) -> Data {
    let dt_gen: DateTime<Utc> = Utc::now();
    let point: Point = position_and_errors();
    Data::new(
        protocol,
        version,
        origin,
        dt_gen,
        point)
}
