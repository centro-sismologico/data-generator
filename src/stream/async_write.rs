use crate::elements::data::Data;
use tokio::io::{self};
use tokio::sync::mpsc::Receiver;


pub async fn stream_data_to_writer<W: io::AsyncWriteExt + std::marker::Unpin>(
    output:&mut  W,
    stream:&mut Receiver<Data>,
    end_flag: &String,
    pretty: &bool,
) -> Result<(), &'static str> {
	match stream.recv().await {
		Some(data) => {
			let mut data_txt = data.json_dumps();
			if !pretty {
				data_txt = serde_json::to_string(&data).unwrap();
			}
			match output.write_buf(&mut data_txt.as_bytes()).await {
				Ok(_) => {
				},
				Err(_err) => {
					return Err("Error on write data")
				}
			}
			match output.write_buf(&mut end_flag.as_bytes()).await {
				Ok(_) => {
				},
				Err(_err) => {
					return Err("Error on write end flag")
				}
			}
		}
		None => {
			return Err("Can't receive data from stream")
		}
	}
	Ok(())
}

#[doc = r#"
Enable an async-writer to stdout\nTake a vector of Data an convert to
stream json
Brings data from stream and receive in a set of bytes. 
convert data to json string.
transform string to bytes and write to output

"#]
pub async fn stream_to_writer<W: io::AsyncWriteExt + std::marker::Unpin>(
    output:&mut W,
    stream: &mut Receiver<Data>,
	iterations: u32,
    //_dataset: &Vec<Data>,
    end_flag: &String,
    pretty: &bool,
) -> Result<u32, io::Error> {
    let mut counter = 0;
	// todo -> restart counter if overflow 
	if iterations==0 {
		loop {
			match stream_data_to_writer(
				output, 
				stream, 
				end_flag, 
				pretty).await {
				Ok(_) => {
					();
				},
				Err(err) => {
					eprintln!("No se pudo escribir {}", err);
					panic!("Error en comunicación");
				}
			}
		}
	} else {
		for _ in 0..iterations {
			match stream_data_to_writer(
				output, 
				stream, 
				end_flag, 
				pretty).await {
				Ok(_) => {
					counter = counter + 1; 
				},
				Err(err) => {
					eprintln!("No se pudo escribir {}", err);
					panic!("Error en comunicación");
				}
			}

		}

	}
	Ok(counter)
}
