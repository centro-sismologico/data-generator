use crate::elements::data::Data;
use serde_json;
use std::fs::File;
use std::io::{self};
use std::path::Path;

#[doc = "\nEnable writer to stdout\nTake a vector of Data an convert to stream json\n"]
pub fn write_io<W: io::Write>(
    mut writer: W,
    dataset: &Vec<Data>,
    end_flag: &String,
    json: bool,
    pretty: bool,
) -> io::Result<()> {
    // iterate over dataset.
    let mut counter = 0;
    if !json {
        dataset.into_iter().for_each(|item| {
            let mut data_txt = item.json_dumps();
            if !pretty {
                data_txt = serde_json::to_string(&item).unwrap();
            }
            match writer.write(data_txt.as_bytes()) {
                Ok(n) => {
                    counter += n;
                }
                Err(_) => {
                    println!("No se pudo escribir {}", item);
                }
            }
            match writer.write(end_flag.as_bytes()) {
                Ok(n) => {
                    counter += n;
                }
                Err(_) => {
                    println!(
                        "No se pudo escribir end flaf {} despues
{}",
                        end_flag, item
                    );
                }
            }
        });
    } else {
        if pretty {
            serde_json::to_writer_pretty(writer, &dataset)?;
        } else {
            serde_json::to_writer(writer, &dataset)?;
        }
    }
    Ok(())
}

#[doc = "\nTake a dataset of Data, convert to json and save on file\nuses write_io with a writer as File, to save,\njson is save as json array (true) or a file with a separator\nreturn a Path\n"]
pub fn write_file(
    dataset: &Vec<Data>,
    filepath: &'static String,
    end_flag: &'static String,
    json: bool,
    pretty: bool,
) -> io::Result<()> {
    // check path parent anf create
    let path = Path::new(filepath);
    let prefix = path.parent().unwrap();
    if !prefix.exists() {
        std::fs::create_dir_all(prefix).unwrap();
    }
    let writer = File::create(filepath)?;
    write_io(writer, dataset, end_flag, json, pretty)?;
    Ok(())
}

#[doc = "\nShow on stdout all the data\n"]
pub fn write_stdout(
    dataset: &Vec<Data>,
    end_flag: &'static String,
    json: bool,
    pretty: bool,
) -> io::Result<()> {
    let writer = io::stdout();
    write_io(writer, dataset, end_flag, json, pretty)?;
    Ok(())
}

/*
Write from stream (not vector) to
file or stdin..

Define a channel to send from (read input) to (write input)
- modo thread
- modo asyncio

En  read -> enviar a channel
En writer -> leer desde channel
 */
