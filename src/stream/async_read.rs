use crate::elements::data::Data;
use std::io::{self};
use tokio::sync::mpsc::Sender;

/*
Read from stream and send to channel

Connect stream to Sender (from channel)
 */

pub async fn reader_to_stream<R: io::BufRead>(
    input: R,
    stream: &Sender<Data>,
    end_flag: &String,
    print: bool,
) {
    // lines is a buffer to accumulate lines
    // then create Data
    let mut lines: Vec<String> = vec![];
    // iteration
    input.lines().into_iter().for_each(|value| {
        match value {
            Ok(line) => {
                // line
                if line.as_str().trim() == end_flag {
                    let new_value = lines.join("");
                    let new_data = Data::json_loads(&new_value);
                    if print {
                        println!("{}", new_data);
                    }
                    let stream = stream.clone();
                    tokio::spawn(async move {
                        if let Err(err) = stream.send(new_data).await {
                            println!("can't send data, {}", err);
                            return;
                        }
                    });
                    lines.clear();
                } else {
                    if !line.trim().is_empty() {
                        lines.push(line);
                    }
                }
                //end accumulation
            }
            Err(_) => {
                println!("Err Valor");
            }
        }
    });
}
