use crate::elements::data::Data;
use serde_json;
use std::error::Error;
use std::fs::File;
//use std::io::stdin;
use std::io::BufReader;
use std::io::{self};
//, Write};
use std::path::Path;

/*
https://doc.rust-lang.org/edition-guide/rust-2018/trait-system/impl-trait-for-returning-complex-types-with-ease.html

dyn use is recommended for Trait objects. When is needed to use.

-> Box<dyn Trait>
"I am returning some type that implements the Trait trait, but I'm not going to tell you exactly what the type is."
First implementation:

 */

#[doc = r#"
    enable reader from stdin
    read every string until symbol [END]
    using read_until
    https://doc.rust-lang.org/std/io/trait.BufRead.html#method.read_until
    cada tanto delimitador, se inclute todos los bytes
    Si no encuentra, lee hasta EOF
    Solo acepta 1 byte
    leer con un límite de arrays (LIMIT const = 10000). (depende como se ejecute)

    Cursor:: implementa BufRead

    Implmentación base:    

    - leer stdin línea a línea
    - acumular en un vector de string
    - cuando llege a 'end_flag'
    - juntar todos los string
    - deserialilzar a Data
    - acumular en vector Data

    Implementación TODO:
    - leer byte a byte (evitar lectura linea a línea)
"#]
#[allow(dead_code)]
pub fn read_io<R: io::BufRead>(
    input: R,
    end_flag: &String,
    print: bool,
) -> Result<Vec<Data>, Box<dyn Error>> {
    let mut lines: Vec<String> = vec![];
    let mut dataset: Vec<Data> = vec![];
    for value in input.lines() {
        let line = value?;
        if line.as_str().trim() == end_flag {
            let new_value = lines.join("");
            let new_data = Data::json_loads(&new_value);
            if print {
                println!("{}", new_data);
            }
            dataset.push(new_data);
            lines.clear();
        } else {
            if !line.trim().is_empty() {
                lines.push(line);
            }
        }
    }
    Ok(dataset)
}

#[allow(dead_code)]
pub fn read_file<P: AsRef<Path>>(path: P) -> Result<Vec<Data>, Box<dyn Error>> {
    // open file if exists path
    let file = File::open(path)?;
    // set file to buffer
    let reader = BufReader::new(file);
    // read the json contents
    let json_data = serde_json::from_reader(reader)?;
    // Return Data
    Ok(json_data)
}
