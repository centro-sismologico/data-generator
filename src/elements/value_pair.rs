/*
derive permite que nuevos ítems sean generados automáticamente
para estructuras de datos. Usa la sintaxis MetaListPaths para especificar
lista de 'rasgos'
 */

use serde::{Deserialize, Serialize};
use std::fmt;
use std::ops;
#[doc = r#"
Una estructura que mantiene un par de valores, usualmente al medir es el
valor y el error de medición.
Ambos son de tipo *f64* y usan los métodos de creación
- ValuePair::empty() :: crea un objeto con valores 0
- ValuePair::new(x,y) :: crea un objeto con valores x e y
- ValuePair::json_dumps() :: crea un string json serializado
- ValuePair::json_loads(json) :: pasa un json a ValuePair"#]
#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct ValuePair {
    pub error: f64,
    pub value: f64,
}

impl ValuePair {
    pub fn empty() -> Self {
        ValuePair {
            error: 0.0,
            value: 0.0,
        }
    }
    pub fn new(value: f64, error: f64) -> Self {
        let _error = error.abs();
        ValuePair {
            value,
            error: _error,
        }
    }

    pub fn json_dumps(&self) -> String {
        serde_json::to_string_pretty(&self).unwrap()
    }

    pub fn json_loads(serialized: &str) -> Self {
        serde_json::from_str(serialized).unwrap()
    }

    pub fn is_zero(&self) -> bool {
        self.value == 0.
    }

    pub fn sqrt(&self) -> Self {
        if self.value >= 0.0 {
            let value_sqrt = self.value.sqrt();
            let error_sqrt = value_sqrt * (0.5 * self.error) / (self.value);
            Self::new(value_sqrt, error_sqrt)
        } else {
            panic!("Value must be positive")
        }
    }

    pub fn pow(&self, n: u16) -> Self {
        let mut base = Self::new(self.value, self.error);
        let vp = Self::new(self.value, self.error);
        for _i in 1..n {
            base *= vp;
        }
        ValuePair::new(base.value, base.error)
    }
}

impl Eq for ValuePair {}

impl fmt::Display for ValuePair {
    /// ValuePair se puede imprimir con formato, tipo __str__ de python
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "value: {} error: {}", self.value, self.error)
    }
}
// Implement PartialEq trait for ValuePair
impl PartialEq for ValuePair {
    /// Permite comparar con otros ValuePair
    fn eq(&self, other: &Self) -> bool {
        self.value == other.value && self.error == other.error
    }

    // fn ne(&self, other: &Self) -> bool {
    //     self.value != other.value && self.error != other.error
    // }
}

impl ops::Add<ValuePair> for ValuePair {
    type Output = ValuePair;

    fn add(self, new_pair: ValuePair) -> Self {
        ValuePair::new(self.value + new_pair.value, self.error + new_pair.error)
    }
}

impl ops::Sub<ValuePair> for ValuePair {
    type Output = ValuePair;

    fn sub(self, new_pair: ValuePair) -> Self {
        ValuePair::new(self.value - new_pair.value, self.error + new_pair.error)
    }
}

impl ops::Mul<ValuePair> for ValuePair {
    type Output = ValuePair;

    fn mul(self, new_pair: ValuePair) -> Self {
        let new_value = self.value * new_pair.value;
        let new_error = (new_value.abs())
            * ((self.error / new_pair.value).powf(2.0)
                + (new_pair.error / self.value).powf(2.0)
                + 2. * (self.error * new_pair.error) / (self.value * new_pair.value))
                .sqrt();
        Self::new(new_value, new_error)
    }
}

impl ops::Div<ValuePair> for ValuePair {
    type Output = ValuePair;

    fn div(self, new_pair: ValuePair) -> Self {
        // if new_pair is zero.
        if !(new_pair.is_zero()) {
            let new_value = self.value / new_pair.value;
            let new_error = (new_value.abs())
                * ((self.error / new_pair.value).powf(2.0)
                    + (new_pair.error / self.value).powf(2.0)
                    - 2. * (self.error * new_pair.error) / (self.value * new_pair.value))
                    .sqrt();
            Self::new(new_value, new_error)
        } else {
            panic!("Division by zero")
        }
    }
}

/*
with mutable objects:
 */
impl ops::MulAssign<ValuePair> for ValuePair {
    fn mul_assign(&mut self, new_pair: ValuePair) {
        // if new_pair is zero.
        let x1 = self.value;
        let x2 = new_pair.value;
        let e1 = self.error;
        let e2 = new_pair.error;
        let x1x2 = x1 * x2;
        let error = (x1x2.abs())
            * ((e2 / x1).powf(2.0) + (e1 / x2).powf(2.0) + 2. * (e1 * e2) / (x1x2.abs())).sqrt();
        self.value *= new_pair.value;
        self.error = error;
    }
}

impl ops::AddAssign<ValuePair> for ValuePair {
    fn add_assign(&mut self, new_pair: ValuePair) {
        // if new_pair is zero.
        self.value += new_pair.value;
        self.error += new_pair.error;
    }
}

impl ops::DivAssign<ValuePair> for ValuePair {
    fn div_assign(&mut self, new_pair: ValuePair) {
        // if new_pair is zero.
        let x1 = self.value;
        let x2 = new_pair.value;
        let e1 = self.error;
        let e2 = new_pair.error;
        let new_value = x1 / x2;

        self.value /= new_pair.value;

        let new_error = (new_value.abs())
            * ((e1 / x2).powf(2.0) + (e2 / x1).powf(2.0) - 2. * (e1 * e2) / (x1 * x2)).sqrt();

        self.error = new_error;
    }
}

impl ops::SubAssign<ValuePair> for ValuePair {
    fn sub_assign(&mut self, new_pair: ValuePair) {
        // if new_pair is zero.
        self.value -= new_pair.value;
        self.error += new_pair.error;
    }
}
