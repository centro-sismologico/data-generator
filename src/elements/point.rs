use super::value_pair::ValuePair;
use serde::{Deserialize, Serialize};
use std::fmt;

#[doc = r#"
Point es la composición de tres *ValuePair*, representando en coordenadas
cartesianas en los tres ejes.

Implementa

- Point::origin() :: crea un punto en (0,0,0)
- Point::new(x,y,z) :: crea un punto en (x,y,z), donde {x,y,x} son ValuePair
- Point::size() :: retorna tamaño del vector (distancia desde origen al punto)
- Point::distance(&Point) :: retorna distancia entre dos puntos
- Point::json_dumps() :: retorna el string json
- Point::json_loads(string_json) :: convierte json en Point
"#]
#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct Point {
    pub x: ValuePair,
    pub y: ValuePair,
    pub z: ValuePair,
}

impl Point {
    pub fn origin() -> Point {
        Point {
            x: ValuePair::empty(),
            y: ValuePair::empty(),
            z: ValuePair::empty(),
        }
    }

    pub fn new(x: ValuePair, y: ValuePair, z: ValuePair) -> Point {
        Point { x, y, z }
    }

    pub fn size(&self) -> ValuePair {
        self.distance(&Self::origin())
    }

    pub fn distance(&self, point: &Self) -> ValuePair {
        let px = self.x - point.x;
        let py = self.y - point.y;
        let pz = self.z - point.z;
        (px.pow(2) + py.pow(2) + pz.pow(2)).sqrt()
    }

    pub fn json_dumps(&self) -> String {
        serde_json::to_string_pretty(&self).unwrap()
    }

    pub fn json_loads(serialized: &str) -> Self {
        serde_json::from_str(serialized).unwrap()
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y && self.z == other.z
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Point(x: {}, y: {}, z: {})", self.x, self.y, self.z)
    }
}
