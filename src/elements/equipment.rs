use serde::{Deserialize, Serialize};
use std::fmt;

#[doc = r#"
*Equipment* define la información del equipo que genera el dato.
Es importante, ya que permite reconocer el dato por el origen y sus
características.

Implementa.

- Equipment::new(name, serie) ::  dada un name(string) y un número de serie
se crea.
- Equipment::json_dumps() :: serializa la estructura a un string json
- Equipment::json_loads() :: deserializa un string json a un Equipment
- Eq1 == Eq2 :: se pueden comparar equipos
- fmt :: formato para string de Equipment
"#]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Equipment {
    pub name: String,
    pub serie_id: u32,
}

impl Equipment {
    pub fn new(name: String, serie: u32) -> Equipment {
        Equipment {
            name,
            serie_id: serie,
        }
    }

    pub fn json_dumps(&self) -> String {
        serde_json::to_string_pretty(&self).unwrap()
    }

    pub fn json_loads(serialized: &str) -> Self {
        serde_json::from_str(serialized).unwrap()
    }
}

impl PartialEq for Equipment {
    fn eq(&self, other: &Self) -> bool {
        self.serie_id == other.serie_id && self.name == other.name
    }
}

impl fmt::Display for Equipment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Equipment(name: {}, serie_id: {})",
            self.name, self.serie_id
        )
    }
}
