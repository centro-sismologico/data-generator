use super::dt_gen_format;
use super::equipment::Equipment;
use super::point::Point;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use std::fmt;
use moebius_tools::time::has_dt_gen::HasDtGen;

#[doc = r#"
Data define la estructura final de un dato que genera un Equipment, agrega
campos para version, protocolo utilizado, estampa de tiempo (dt_gen)

Implementa

- Data::new(protocol, version, origin, dt_gen, point) :: crea un dato dadas las entradas
- Data::json_dumps() :: retorna el string json
- Data::json_loads(string_json) :: convierte json en Point
"#]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Data {
    #[serde(with = "dt_gen_format")]//module
    pub dt_gen: DateTime<Utc>,
    pub origin: Equipment,
    pub point: Point,
    pub protocol: String,
    pub version: String,
}

impl HasDtGen for Data {
	fn get_dt_gen(&self) -> DateTime<Utc> {
		self.dt_gen
	}	
}

impl fmt::Display for Data {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Data(protocol: {}, version: {}, origin: {}, dt_gen {}, point {})",
            self.protocol, self.version, self.origin, self.dt_gen, self.point
        )
    }
}

impl Data {
    /*
    create a new data
     */

    pub fn new(
        protocol: &String,
        version: &String,
        origin: &Equipment,
        dt_gen: DateTime<Utc>,
        point: Point,
    ) -> Data {
        let cloned_origin = origin.clone();
        Data {
            protocol:protocol.to_string(),
            version:version.to_string(),
            origin:cloned_origin,
            dt_gen,
            point,
        }
    }

    pub fn json_dumps(&self) -> String {
        serde_json::to_string_pretty(&self).unwrap()
    }


    pub fn json_to_value(&self) -> Result<serde_json::Value,String> {
        match serde_json::to_value(self) {
			Ok(val)=>Ok(val),
			Err(e)=>Err(e.to_string())
		}
    }


    pub fn json_from_value(value:&serde_json::Value) -> Self {
        serde_json::from_value(value.clone()).unwrap()
    }
	
	

    pub fn json_loads(serialized: &str) -> Self {
        serde_json::from_str(serialized).unwrap()
   }

}

pub fn test_dt_gen<DT:HasDtGen>(v:&DT) {
	println!("{:?}",v.get_dt_gen());
}

pub fn test_data(v:&Data){
	test_dt_gen(v);
}


impl PartialEq for Data {
    fn eq(&self, other: &Self) -> bool {
        self.protocol == other.protocol
            && self.version == other.version
            && self.origin == other.origin
            && self.dt_gen == other.dt_gen
            && self.point == other.point
    }
}
