/*
TODO ::

- modularizar en archivos
- pretty print data
- Data -> Serializar
- hacer pruebas -> test
- publicar
*/

//named:
//

use datagen_gnss::elements::{data::Data, equipment::Equipment};
use datagen_gnss::generators::create_data;
use moebius_tools::time::has_dt_gen::HasDtGen;


#[tokio::main]
async fn main() {
    // fields : SOURCE, ID, DATETIME, POSITION, ERRORS
    let protocol: String = String::from("gendata");
    let version: String = String::from("v2021.06.25");
    let equipment_name: String = String::from("TestData");
    let id: u32 = 1;
    let equipment: Equipment = Equipment::new(equipment_name, id);
    let data: Data = create_data(
        &protocol,
        &version,
        &equipment);
    // let value = random_value(-3.00,5.6);
    // let point: Point = position_and_errors();
    println!("{}", data);
    println!("Data serializado a JSON::");
    println!("{}", data.json_dumps());
    println!("Ahora pasamos un string a 'data'");
    let json_data = r#"
                {
                  "protocol": "gendata",
                  "version": "v2021.06.25",
                  "origin": {
                    "name": "TestData",
                    "serie_id": 1
                  },
                  "dt_gen": "08/07/2021T15:31:25.775178267UTC",
                  "point": {
                    "x": {
                      "value": 2.927140579832943,
                      "error": 0.3108550010025529
                    },
                    "y": {
                      "value": 4.931106613063515,
                      "error": 0.3372219197557157
                    },
                    "z": {
                      "value": -2.6833218306846423,
                      "error": 0.49323631442951354
                    }
                  }
                }"#;

    let new_data: Data = Data::json_loads(json_data);
	let dt_gen = new_data.get_dt_gen();
    println!("{}", new_data);
	println!("dtgen {}",dt_gen);
}
